variable "environment" { type = string }
variable "region" { type = string }
variable "user" {
  type    = string
  default = "michael"
}

locals {
  default_tags = {
    cg_iac           = "terraform"
    cg_env           = var.environment
    cg_product_id    = "APP-CAMP-MYE"
    cg_product       = "Training Lambda"
    cg_release_state = "N/A"
    cg_business_unit = "EM"
    cg_team          = "myelefant-campaigns@sinch.com"
  }
}
