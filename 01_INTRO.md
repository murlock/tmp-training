# Lambda


Lambda is :
- serverless 
- write your function


Pros:
- scale from 0 to 100
- no cost when unused
- IAM role

Cons:
- short lived (be aware of duration of your function)
- cost based on memory / cpu / time (may reserve some surprise !)
- require a minimal bootstrap


Lambda has a cold start where the first request can have degregated TTFB 
- minimal time to bootstrap your function
- minimal time to initialize your function

After first launch, a lambda is kept ready to serve new requests



Several Usage of Lambda:

- With API Gateway:
    - serve HTTP Requests
    - manage custom authentication

- Events from AWS
    - react on S3 uploaded object (compute, create thumbnail, add tags, ...)
    - parse cloudwatch logs
    - ...


Step Functions is a mecanism to have a workflow of lambda
- Beware of cost


Lambda support several languages:
    - Python
    - Java
    - Nodejs
    - dotNet
    - Go
    - Ruby
    - custom (using Lambda container)
    
**Python oriented**

Lambda is composed of layers (dependencies that could be updated without change on code or code without uploading layer)
- each layer is a zip
- each layer are unzipped regarding the order
- code function may also an uploaded zip (or a link to S3 object)

code: handle have two parameters:
- event: the payload received
- context https://docs.aws.amazon.com/lambda/latest/dg/python-context.html
    - get_remaining_time_in_millis()
    - function_name
    - ...
- also environmment
