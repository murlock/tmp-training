import json
import os

def handler(event, context):
    message = "Hello World by %s and my name is %s" % \
        (os.environ.get("user", "John Doe"), context.function_name)
    return {
        "statusCode": 200,
        "headers": {"Content-Type": "application/json"},
        "body": json.dumps({"message": message})
    }
