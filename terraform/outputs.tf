output "lambda_url" {
  value = aws_lambda_function_url.test_latest.function_url
}

output "api_gateway_stage" {
    value = aws_apigatewayv2_stage.lambda.invoke_url
}
