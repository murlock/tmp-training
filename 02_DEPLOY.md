# How to deploy Lambda

- With CDK
    - conv-api-stats-aggregator (deprecated version)
- With serverless
    - staging-slack-bot (issue with AWS SSO)
- With Terraform
    - this exemple


**NOTE** This example will requires you to have:
- terraform
- to destroy your resource at the end of training
- terraform backend is local to simplify


## Terraform

```
$ terraform init

$ terraform plan -var-file=envs/mlft-staging-eu.tfvars
```


## The Lambda




## The API Gateway

